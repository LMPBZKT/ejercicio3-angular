import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {


  constructor() { }
  valor1=200;
  valor2=200;

  ngOnInit(): void {
  }
  calculo():number{
    return this.valor1+this.valor2;
  }
}
